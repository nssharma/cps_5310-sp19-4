function [error] = directScript( )
initGuess = [0.0036; 0.9395];
[p, error] = fminsearch(@directmethError, initGuess);
days = 0:12;
S = [762 740 650 400 250 120 80 50 20 18 15 13 10];
I = [1 20 80 220 300 260 240 190 120 80 20 5 2];
[t, y] = ode45(@directmeth, [0,12], [S(1); I(1)],[], p);
subplot(2,1,1);
plot(t, y(:,1), days, S, 'o')
title('Susceptible population, model and data','FontSize',11)

subplot(2,1,2);
plot(t, y(:,2), days, I, 'o')
title('Infected population, model and data','FontSize',11)
