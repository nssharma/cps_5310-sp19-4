% Fitting a model to U.S. population data
% Author: Aryal Bibek

% read data %
infile = "./skills_exam/us_population.txt";
data = load(infile, "-ascii");
x = data(:,1);
y = data(:,2);
x = x - 1790; % reduce initial x to 0

% initial guess for parameters %
y0 = y(1);
r = 0.01;       %assumption for r
K = 2*max(y);   % we assume carrying capacity is twice the max population
p0 = [y0 r K];

%define function
exact_solution = @(p,x)p(3)*p(1)./(p(1)+(p(3)-p(1))*exp(-p(2)*x));

%Nonlinear Least Squares Best Fit
[p,Error] = lsqcurvefit(exact_solution,p0,x,y);
N = exact_solution(p,x);

x = x + 1790; % make initial x back to 1740 for plotting
hold on;
plot(x,y,'ro',x,N,'g')
xlabel('Years');
ylabel('Population');
legend('Data','lsqcurvefit');
title("U.S. Population data");
hold off;

